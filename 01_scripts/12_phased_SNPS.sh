#!/bin/bash

##############  Script to phase SNPs  #################

#Optional SLURM directives for users on HPC systems
#SBATCH --job-name=phase_SNP
#SBATCH --partition=ecobio
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=20
#SBATCH --time=5:00:00 
#SBATCH --mem=100G 
#SBATCH --output=phase_SNP_%j.out
#SBATCH --error=phase_SNP_%j.err

# Load conda environment with harpy, to be changed with your own environment created before. 
source /local/env/envconda.sh
conda activate harpy_test

# VARIABLES
GENOME="02_genome/genome.fasta"
INPUT_DATA="SNP/Filtered_SNPS/filtered_snps.vcf"
ALIGNEMENT_DATA="07_alignement_on_reference_bwa/Align/bwa/"
	
# Run harpy phase
harpy phase --threads 20 --vcf $INPUT_DATA --directory $ALIGNEMENT_DATA

conda deactivate

#!/bin/bash

##############  Script to align the reads on reference genome using bwa  #################

#Optional SLURM directives for users on HPC systems
#SBATCH --job-name=harpy_b_align
#SBATCH --partition=genouest
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=20
#SBATCH --time=5:00:00 
#SBATCH --mem=100G 
#SBATCH --output=bwa_%j.out
#SBATCH --error=bwa_%j.err

# Load conda environment with harpy, to be changed with your own environment created before. The content of this environment: "03_environments/env_harpy_13_57.yml"
. /local/env/envconda.sh
conda activate env_harpy_13_57

# VARIABLES
GENOME="02_genome/genome.fasta"
INPUT_DATA="06_QC"

	
# Run harpy align bwa
harpy align bwa --genome $GENOME --directory $INPUT_DATA --quality-filter 20 --threads 20
	
conda deactivate

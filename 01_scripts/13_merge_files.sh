#!/bin/bash

################### Script to merge bam files for re run leviathan ###################

#Optional SLURM directives for users on HPC systems
#SBATCH --job-name=Merge
#SBATCH --partition=genouest
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=2
#SBATCH --time=1:00:00
#SBATCH --mem=5G
#SBATCH --output=Merge_%j.out
#SBATCH --error=Merge_%j.err

# load conda environment
. /local/env/envconda.sh
conda activate samtool_env

# VARIABLES
BAM_DATA="07_alignement_on_reference_bwa/Align/bwa/"
CAT_DIR="11_cat_bam/"

# Merge BAM files using samtools merge
echo "Merging BAM files..."
if samtools merge ${CAT_DIR}/merged_data.bam ${BAM_DATA}/*.bam; then
  echo "BAM files merged successfully."
else
  echo "Error in merging BAM files."
  exit 1
fi

# Index the merged BAM file
echo "Indexing the merged BAM file..."
if samtools index ${CAT_DIR}/merged_data.bam; then
  echo "BAM file indexed successfully."
else
  echo "Error in indexing BAM file."
  exit 1
fi

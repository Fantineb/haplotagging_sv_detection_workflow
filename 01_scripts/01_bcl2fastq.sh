#!/bin/bash

##############  Script to generate fastq.gz files from bcl files #################

#Optional SLURM directives for users on HPC systems
#SBATCH --job-name=bcl2fastq_conversion
#SBATCH --partition=genouest
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=40 
#SBATCH --time=48:00:00 
#SBATCH --mem=50G
#SBATCH --output=bcl2fastq_%j.out
#SBATCH --error=bcl2fastq_%j.err

# Load conda environment with bcf2fastq, to be changed with your own environment created before. The content of this environment: "03_environments/bcf2fastq.yml"
. /local/env/envconda.sh
conda activate bcl2fastq

# VARIABLES
OUTPUT_DIR="04_raw_data"
RAW_DATA_DIR="/scratch/fbenoit/Haplotagging/230412_A00924_0406_AH2CFKDRX3" 

# Run bcl2fastq
bcl2fastq --use-bases-mask=Y51,I13,I13,Y51 \
          --create-fastq-for-index-reads \
          -r 20 -w 20 -p 20 \
          -R $RAW_DATA_DIR \
          --tiles s_[1-2] \
          --output-dir=$OUTPUT_DIR \
          --interop-dir=$RAW_DATA_DIR/InterOp \
          --reports-dir=$OUTPUT_DIR \
          --no-lane-splitting

conda deactivate


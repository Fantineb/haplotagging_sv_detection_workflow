#!/bin/bash

##############  Script to detect Structural variants using Leviathan  #################

#Optional SLURM directives for users on HPC systems
#SBATCH --job-name=harpy_SV
#SBATCH --partition=genouest
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=20
#SBATCH --time=24:00:00 
#SBATCH --mem=100G 
#SBATCH --output=SV_levia_%j.out
#SBATCH --error=SV_levia_%j.err

# Load conda environment with harpy, to be changed with your own environment created before. The content of this environment: "03_environments/harpy_test"
source /local/env/envconda.sh
conda activate harpy_test

# VARIABLES
GENOME="02_genome/genome.fasta"
INPUT_DATA="Align/bwa/"
	
# Run harpy align bwa
harpy sv leviathan --threads 20 --genome $GENOME --extra-params "-r 10000 -v 1000 -s 97 -m 97 -l 97" $INPUT_DATA
	
conda deactivate

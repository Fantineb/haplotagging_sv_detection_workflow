#!/bin/bash

##############  Script to trim and perform QC on demultiplexed data #################

#Optional SLURM directives for users on HPC systems
#SBATCH --job-name=harpy_qc
#SBATCH --partition=genouest
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=20
#SBATCH --time=1:00:00 
#SBATCH --mem=100G 
#SBATCH --output=harpy_qc%j.out
#SBATCH --error=harpy_qc%j.err

# Load conda environment with harpy, to be changed with your own environment created before. The content of this environment: "03_environments/env_harpy_13_57.yml"
. /local/env/envconda.sh
conda activate env_harpy_13_57

# VARIABLES
DATA_DIR="05_raw_data_demultiplexed"

# Run HARPY QC
harpy qc --directory "${DATA_DIR}" --max-length 51 --threads 20

conda deactivate

#!/bin/bash

##############  Script to run wrath using haplotagging data  #################

#Optional SLURM directives for users on HPC systems
#SBATCH --job-name=wrath
#SBATCH --partition=genouest
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=20 
#SBATCH --time=4:00:00 
#SBATCH --mem=50G
#SBATCH --output=WRATHPOP_%j.out
#SBATCH --error=WRATHPOP_%j.err

# Load environment
source /local/env/envsamtools-1.6.sh
source /local/env/envbedtools-2.27.1.sh
source /local/env/envr-4.1.3.sh
source /local/env/envhtslib-1.6.sh
source /scratch/fbenoit/haplotagging_sv_detection_workflow/03_environments/bin/activate

# VARIABLES
MATRIX1="wrath_out/matrices/jaccard_matrix_40000_LG1_9000000_31000000_wrath_BB_LG1.txt"
MATRIX2="wrath_out/matrices/jaccard_matrix_40000_LG1_9000000_31000000_wrath_AA_LG1.txt"
WINDOW_FILE="window_file.tsv"  
OUTPUT_FILE="10_wrath_out/wrath_pop/"

export PATH=$PATH:/scratch/fbenoit/wrath/wrath 

# Run wrath

/scratch/fbenoit/wrath/sv_detection/plot_2matrices_together.py -m1 $MATRIX1 -m2 $MATRIX2 -o $OUTPUT_FILE -w $WINDOW_FILE

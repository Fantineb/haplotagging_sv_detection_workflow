#!/bin/bash

##############  Script to filter SNPs  #################

#Optional SLURM directives for users on HPC systems
#SBATCH --job-name=Filter_SNP
#SBATCH --partition=ecobio
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=20
#SBATCH --time=5:00:00 
#SBATCH --mem=100G 
#SBATCH --output=Filter_SNP_%j.out
#SBATCH --error=Filter_SNP_%j.err

# Load conda environment with harpy, to be changed with your own environment created before.
source /local/env/envconda.sh
conda activate bcftools_env

# VARIABLES
INPUT_DATA="SNP/mpileup/variants.raw.bcf"
OUTPUT_DATA="SNP/Filtered_SNPS/filtered_snps.vcf"
	
# Run bcftools
bcftools filter -i 'QUAL>95' -o $OUTPUT_DATA $INPUT_DATA

conda deactivate

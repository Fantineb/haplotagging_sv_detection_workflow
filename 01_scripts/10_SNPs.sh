#!/bin/bash

##############  Script to detect SNPs  #################

#Optional SLURM directives for users on HPC systems
#SBATCH --job-name=SNP
#SBATCH --partition=ecobio
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=20
#SBATCH --time=4:00:00 
#SBATCH --mem=100G 
#SBATCH --output=SNP_%j.out
#SBATCH --error=SNP_%j.err

# Load conda environment with harpy, to be changed with your own environment created before. The content of this environment: "03_environments/harpy_test"
source /local/env/envconda.sh
conda activate harpy_test

# VARIABLES
GENOME="02_genome/genome.fasta"
#INPUT_DATA="07_alignement_on_reference_bwa/Align/bwa/"
INPUT_DATA="11_cat_bam/"
	
# Run harpy align bwa
harpy snp mpileup --threads 20 --genome $GENOME $INPUT_DATA

conda deactivate

#!/bin/bash

##############  Script to test fastq file quality after demultiplex #################

#Optional SLURM directives for users on HPC systems
#SBATCH --job-name=harpy_preflight
#SBATCH --partition=genouest
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=20
#SBATCH --time=1:00:00 
#SBATCH --mem=15G 
#SBATCH --output=harpy_preflight%j.out
#SBATCH --error=harpy_preflight%j.err

# Load conda environment with harpy, to be changed with your own environment created before. The content of this environment: "03_environments/env_harpy_13_57.yml"
. /local/env/envconda.sh
conda activate harpy_test

# VARIABLES
RAW_DATA="06_QC/" 
OUTPUT="Preflight/bam"

# Run HARPY preflight
harpy preflight bam --output-dir $OUTPUT --threads 20 $RAW_DATA

conda deactivate

#!/bin/bash

############# Script to turn BCF files into VCF  #########
#############        LOOP version       ##########

#Optional SLURM directives for users on HPC systems
#SBATCH --job-name=SV_filtered
#SBATCH --partition=genouest
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=2
#SBATCH --time=1:00:00
#SBATCH --mem=5G
#SBATCH --output=SV_filtered%j.out
#SBATCH --error=SV_filtered%j.err

# Load conda environment with bcftools
. /local/env/envconda.sh
conda activate bcftools_env

# Variables
OUTPUT_VCF_DIR="08_SVs_calling_Leviathan_bwa/SVs_leviathan_r10000_v1000/leviathan"
SAMPLE_LIST="sample_list_bis.txt"
INPUT_BCF_DIR="08_SVs_calling_Leviathan_bwa/SVs_leviathan_r10000_v1000/leviathan"

for SAMPLE in $(cat $SAMPLE_LIST); do
    INPUT_BCF="${INPUT_BCF_DIR}/${SAMPLE}.bcf"
    FILTERED_VCF="${OUTPUT_VCF_DIR}/${SAMPLE}_filtered.vcf"
    bcftools view "$INPUT_BCF" | bcftools sort | bcftools view -i 'SVTYPE="INV"' -O v -o "$FILTERED_VCF"
    echo "Terminated for $SAMPLE : $FILTERED_VCF"
done
   

conda deactivate

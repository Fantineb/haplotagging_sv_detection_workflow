#!/bin/bash


#Optional SLURM directives for users on HPC systems
#SBATCH --job-name=stats
#SBATCH --partition=genouest
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#SBATCH --time=1:00:00
#SBATCH --mem=10G
#SBATCH --output=stat_%j.out
#SBATCH --error=stat_%j.err

# VARIABLES
SAMPLE="Mouche8-BB22x10"
GENOME="02_genome/genome.fasta"
ALIGNED_DIR="Align/bwa"
BAM_FILE="$ALIGNED_DIR/$SAMPLE.bam"

. /local/env/envconda.sh
conda activate samtool_env

#Alignment statistics
samtools stats $BAM_FILE > $ALIGNED_DIR/"$SAMPLE"_mapping.stats

samtools coverage $BAM_FILE > $ALIGNED_DIR/"$SAMPLE"_coverage.txt


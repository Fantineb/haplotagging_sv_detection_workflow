#!/bin/bash

##############  Script to run wrath using haplotagging data  #################

#Optional SLURM directives for users on HPC systems
#SBATCH --job-name=wrath
#SBATCH --partition=genouest
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=20 
#SBATCH --time=4:00:00 
#SBATCH --mem=50G
#SBATCH --output=wrath_%j.out
#SBATCH --error=wrath_%j.err

# Load environment
source /local/env/envsamtools-1.6.sh
source /local/env/envbedtools-2.27.1.sh
source /local/env/envr-4.1.3.sh
source /local/env/envhtslib-1.6.sh
source /scratch/fbenoit/haplotagging_sv_detection_workflow/03_environments/bin/activate

pip install pysam scikit-learn

# VARIABLES
GENOME="02_genome/genome.fasta"
CHROM="LG1"
WINDOW="40000"
LIST="10_wrath_out/wrath_bamfiles/wrath_AA_LG1.txt"
THREADS="20"
START="9000000"
END="31000000"

export PATH=$PATH:/scratch/fbenoit/wrath/wrath 

# Run wrath
/scratch/fbenoit/wrath/wrath -g $GENOME -c $CHROM -w $WINDOW -a $LIST -t $THREADS -l -s $START -e $END 

#!/bin/bash

##############  Script to detect Structural variants using Naibr  #################

#Optional SLURM directives for users on HPC systems
#SBATCH --job-name=Naibr
#SBATCH --partition=ecobio
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=20
#SBATCH --time=5:00:00 
#SBATCH --mem=100G 
#SBATCH --output=Naibr_%j.out
#SBATCH --error=Naibr_%j.err

# Load conda environment with harpy, to be changed with your own environment created before. The content of this environment: "03_environments/harpy_test"
source /local/env/envconda.sh
conda activate harpy_test

# VARIABLES
GENOME="02_genome/genome.fasta"
INPUT_DATA="07_alignement_on_reference_bwa/Align/bwa/"
PHASED="13_Phase/variants.phased.bcf"
	
# Run harpy sv naibr
harpy sv naibr --threads 20 -x "min_sv 1000" --genome $GENOME --vcf $PHASED $INPUT_DATA
	
conda deactivate

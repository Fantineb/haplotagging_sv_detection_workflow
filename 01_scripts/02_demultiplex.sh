#!/bin/bash

####### Script to perform molecular and individual demultiplexing using harpy ########

#Optional SLURM directives for users on HPC systems
#SBATCH --job-name=harpy_demultiplex
#SBATCH --partition=ecobio
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=20  
#SBATCH --mem=200G  
#SBATCH --time=300:00:00
#SBATCH --output=harpy_%j.out
#SBATCH --error=harpy_%j.err
#SBATCH -o log_%j

# Load conda environment with harpy, to be changed with your own environment created before. The content of this environment: "03_environments/harpy.yml"
. /local/miniconda3/etc/profile.d/conda.sh
conda activate env_harpy_13_57

# VARIABLES
INPUT_DIR="04_raw_data" 
INPUT_FILE="Undetermined_S0_R1_001.fastq.gz"

# Run Harpy
harpy demultiplex gen1 --threads ${SLURM_CPUS_PER_TASK} --file $INPUT_DIR/$INPUT_FILE --samplesheet sampleSheet.txt


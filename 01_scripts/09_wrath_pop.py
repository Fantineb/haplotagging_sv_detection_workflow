#!/usr/bin/env python

# Define parameters
start = 9000000
end = 31000000
window_size = 40000

# Generate windows
windows = []
for i in range(start, end, window_size):
    window_end = min(i + window_size - 1, end)
    windows.append([i, window_end])

# Write to file
with open('window_file.tsv', 'w') as f:
    for window in windows:
        f.write("{0}\t{1}\n".format(window[0], window[1]))


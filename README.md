# Haplotagging_SV_detection_workflow

## Global context of the workflow

The objectif of this pipeline is to use short-barcoded reads from haplotagging (linked-read) to detect structural variants, more specifically, chromosomal inversions.

This pipeline use haplotagging data from 17 samples of *coelopa frigida*, a seafly species as raw data. This species has 5 chromsomes and 1 small sexual one, and is known to have several large chromosomal inversions in it genome, such as on chromosome 1.

The problem here is to detect precisely the inversions and their specific breakpoints.
Using ONT and haplotagging data (see the [ONT pipeline](https://gitlab.com/Fantineb/ont_sv_detection_workflow)) in parallel, we aim to characterise the localization of chromosomal inversions in this species.

## Prerequisites

For this project we used the Genouest cluster (for more information, see [general documentation](https://www.genouest.org/2017/03/02/cluster/)). Its aim is to provide access to many  bioinformatics resources and to run scripts, using a linux base.
Access to the cluster is via an ssh key. Once connected, the user can request the infrastructure via the SLURM submission system (for more information on SLURM, see [SLURM info](https://help.genouest.org/usage/slurm/)).

The computer used to run the analysis:

## How to use

Clone this repository to your local machine:

```git clone [repo-link]```

## Architecture of the project
This repository is composed by several and specific parts. 

* **01_scripts**: contains all scripts numbered in order of use. For almost each step, two scripts has been used. First an indi
* **02_genome**: contains the genome of reference used for alignment
* **03_environments**: contains all the environments used in this pipeline, each environement can be composed by severals dependant tools in a yaml file
* **04_raw_data**: contains the raw data send by the sequencing platform 

The other repository are described hereafter.


## Pipeline
### Overview

Step-by-step pipeline to detect chromosomal inversions using haplotagging data
IMAGE Pipeline

### Detailed steps

Here are all the steps implemented with each tool used:

The raw data are contained in one unique bcl file, we need to turn the bcl file into a fastq file:

#### Step 1. BCL to fastq data:

The BCL file is transformed into fastq file using "01_bcl2fastq.sh" script in `01_scripts` repository

* **Input data**: one `bcl` file containing all samples

* **Output data**: one `fastq` file containing all samples

Fastq file is saved in `04_raw_data`repository.

From the next step, [Harpy](https://github.com/pdimens/HARPY) (v0.9) was used. It is a serie of commands that realize the haplotagging pipeline.

#### Step 2. Molecular and individual demultiplexing:

Demultiplexing is performed with `harpy demultiplex`.

The scripts used is `02_demultiplex.sh` in `01_scripts` repository. 

* **Input data**: one `fastq` file

* **Output data**: several `fastq` file for each individuals (*R.fastq for reverse; *F.fastq for forward reads)

Demultiplexed data are saved in `05_raw_data_demultiplexed` repository.

#### Step 3. Quality control:

Quality control is performed using `harpy QC` command.

The script used is `03_harpy_QC.sh` in `01_scripts` repository. 

* **Input data**: 2 `fastq.gz` file per sample

* **Output data**: QC files in `.html` format

Quality control data are saved in `06_QC` repository.

#### Step 4. Alignment on reference genome:

Alignment is performed by the command harpy align BWA and Harpy align EMA (not done yet).

The script used is `05_alignment_on_reference_bwa.sh` in `01_scripts` repository.

* **Input data**: `fastq.gz` files

* **Output data**: `.bam`, `.bai` and `.html` for the reports

Alignment data are saved in `07_alignement_on_reference_bwa` repository.

#### Step 5. Structural variant detection with LEVIATHAN and NAIBR    

SV calling is performed by harpy sv leviathan and Naibr separetely.

The scripts used are `06_SV_calling_leviathan.sh` and `06b_SV_calling_naibr.sh` in `01_scripts` repository.

* **Input data**: `.bam` data from alignement

* **Output data**: `.bcf` `.html` `.bcf` 

SVs data are saved in and `08_SVs_calling_Leviathan_bwa`; `09_SV_naibr` repositories.

For Naibr preliminary steps are necessary:

the scripts used: `10_SNPs.sh`; `11_bcftools.sh`; `12_phased_SNPS.sh`
The outputs are stored in `11_cat_bam`; `12_SNP

#### Step 6. Visualization with Wrath

The visualization of the barcode sharing between populations is performed by Wrath

The scripts used are `07_wrath.sh`; `09_wrath_pop.sh`; `09_wrath_pop.py`; 
The outputs are stored in `10_wrath_out`

#### Step 7. Formatting

Script to merge bam files for the Leviathan pooled perspective: "13_merge_files.sh"
Script to transform BCF files into VCF files: "bcf_to_vcf_LOOP.sh"

## Authors

[Fantine Benoit](https://gitlab.com/Fantineb)

Master students in bioinformatics at Rennes 1 University (Master mention Bio-informatique parcours Informatique pour la biologie et la santé).
This work has been realized during an internship at Ecobio laboratory, for [Claire Mérot](https://github.com/clairemerot) and [Claire Lemaitre](https://github.com/clemaitre), from Feb to July 2024.


## License

